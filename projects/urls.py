from django.urls import path
from . import views
from django.views.generic import RedirectView

urlpatterns = [
    path("", RedirectView.as_view(pattern_name="list_projects"), name="home"),
    path("projects/", views.list_projects, name="list_projects"),
    path("<int:id>/", views.show_project, name="show_project"),
    path("projects/create/", views.create_project, name="create_project"),
]
