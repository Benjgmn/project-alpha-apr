from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150, label="username")
    password = forms.CharField(
        max_length=150, label="password", widget=forms.PasswordInput()
    )


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150, label="username")
    password = forms.CharField(
        max_length=150, label="password", widget=forms.PasswordInput()
    )
    password_confirmation = forms.CharField(
        max_length=150,
        label="password_confirmation",
        widget=forms.PasswordInput(),
    )
